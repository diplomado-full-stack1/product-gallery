"use client";

import React from 'react';
import { Product } from '../types';
import { Button } from '@mui/material';
import axiosInstance from '../utils/axiosInstance';

type ProductDetailProps = {
  product: Product;
};

const ProductDetail: React.FC<ProductDetailProps> = ({ product }) => {
  const deleteProduct = async () => {
    try {
      await axiosInstance.delete(`/api/products/${product.id}`);
      alert('Product deleted');
    } catch (error) {
      console.error('Product deletion failed', error);
    }
  };

  return (
    <div>
      <h2>{product.name}</h2>
      <p>{product.description}</p>
      <p>{product.price}</p>
      <Button onClick={deleteProduct} variant="contained" color="secondary">
        Delete Product
      </Button>
    </div>
  );
};

export default ProductDetail;
