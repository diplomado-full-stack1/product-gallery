"use client";

import React from 'react';
import Link from 'next/link';
import { Product } from '../types';

type ProductListProps = {
  products: Product[];
};

const ProductList: React.FC<ProductListProps> = ({ products }) => {
  return (
    <div>
      {products.map((product) => (
        <div key={product.id}>
          <h2>{product.name}</h2>
          <p>{product.description}</p>
          <p>{product.price}</p>
          <Link href={`/${product.id}`}>
            View Details
          </Link>
        </div>
      ))}
    </div>
  );
};

export default ProductList;
