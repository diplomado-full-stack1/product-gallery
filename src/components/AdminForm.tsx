import { useForm } from 'react-hook-form';
import { TextField, Button } from '@mui/material';
import axios from 'axios';
import { useEffect } from 'react';

type AdminFormProps = {
  productId?: string;
  initialData?: { name: string; description: string; price: number };
};

const AdminForm = ({ productId, initialData }: AdminFormProps) => {
  const { register, handleSubmit, setValue } = useForm();

  useEffect(() => {
    if (initialData) {
      setValue('name', initialData.name);
      setValue('description', initialData.description);
      setValue('price', initialData.price);
    }
  }, [initialData, setValue]);

  const onSubmit = async (data: any) => {
    try {
      if (productId) {
        // Editar producto
        await axios.put(`/api/products/${productId}`, data);
        alert('Product updated');
      } else {
        // Crear producto
        await axios.post('/api/products', data);
        alert('Product created');
      }
    } catch (error) {
      console.error('Error submitting form', error);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField {...register('name')} label="Name" variant="outlined" fullWidth margin="normal" />
      <TextField {...register('description')} label="Description" variant="outlined" fullWidth margin="normal" />
      <TextField {...register('price')} label="Price" variant="outlined" fullWidth margin="normal" />
      <Button type="submit" variant="contained" color="primary">
        {productId ? 'Update Product' : 'Create Product'}
      </Button>
    </form>
  );
};

export default AdminForm;