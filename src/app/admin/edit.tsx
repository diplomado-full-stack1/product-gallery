import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import axios from 'axios';
import AdminForm from '../../components/AdminForm';

const EditProductPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [productData, setProductData] = useState(null);

  useEffect(() => {
    if (id) {
      const productId = Array.isArray(id) ? id[0] : id;
      axios.get(`/api/products/${productId}`)
        .then(response => setProductData(response.data))
        .catch(error => console.error(error));
    }
  }, [id]);

  const productId = Array.isArray(id) ? id[0] : id;

  return (
    <div>
      <h1>Edit Product</h1>
      {productData ? <AdminForm productId={productId} initialData={productData} /> : <p>Loading...</p>}
    </div>
  );
};

export default EditProductPage;
