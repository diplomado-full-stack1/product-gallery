import AdminForm from '../../components/AdminForm';

const CreateProductPage = () => {
  return (
    <div>
      <h1>Create Product</h1>
      <AdminForm />
    </div>
  );
};

export default CreateProductPage;