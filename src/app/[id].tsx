import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import axios from 'axios';
import ProductDetail from '../components/ProductDetail';

const ProductPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [product, setProduct] = useState(null);

  useEffect(() => {
    if (id) {
      axios.get(`/api/products/${id}`)
        .then(response => setProduct(response.data))
        .catch(error => console.error(error));
    }
  }, [id]);

  return (
    <div>
      {product ? <ProductDetail product={product} /> : <p>Cargando...</p>}
    </div>
  );
};

export default ProductPage;
