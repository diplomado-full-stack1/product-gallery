"use client";

import { useEffect, useState } from 'react';
import axiosInstance from '../utils/axiosInstance';
import ProductList from '../components/ProductList';
import { Product } from '../types';

const HomePage = () => {
  const [products, setProducts] = useState<Product[]>([]);

  useEffect(() => {
    axiosInstance.get('/api/products')
      .then(response => setProducts(response.data))
      .catch(error => console.error(error));
  }, []);

  return (
    <div>
      <h1>Productos</h1>
      <ProductList products={products} />
    </div>
  );
};

export default HomePage;
