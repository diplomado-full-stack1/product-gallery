import { useForm } from 'react-hook-form';
import { TextField, Button } from '@mui/material';
import axios from 'axios';

const LoginPage = () => {
  const { register, handleSubmit } = useForm();

  const onSubmit = async (data: any) => {
    try {
      const response = await axios.post('/api/login', data);
      console.log('Login successful', response.data);
    } catch (error) {
      console.error('Login failed', error);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField {...register('username')} label="Username" variant="outlined" />
      <TextField {...register('password')} label="Password" type="password" variant="outlined" />
      <Button type="submit" variant="contained">Login</Button>
    </form>
  );
};

export default LoginPage;
