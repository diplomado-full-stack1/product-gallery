import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'http://localhost:4000', // La URL base de tu backend
});

export default axiosInstance;
