describe('Delete Product', () => {
    it('should delete a product successfully', () => {
      cy.login();
      cy.visit('/admin');
      cy.get('button[data-cy=delete-product-1]').click(); // ID del producto a eliminar
      cy.contains('Product deleted successfully');
    });
  });