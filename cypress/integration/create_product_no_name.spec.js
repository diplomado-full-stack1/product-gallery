describe('Create Product without Name', () => {
    it('should show an error when the product name is missing', () => {
      cy.login();
      cy.visit('/admin/create');
      cy.get('input[name=price]').type('100');
      cy.get('button[type=submit]').click();
      cy.contains('Product name is required');
    });
  });