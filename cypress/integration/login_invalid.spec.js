describe('Invalid Login Flow', () => {
    it('should show an error message with invalid credentials', () => {
      cy.visit('/login');
      cy.get('input[name=username]').type('invalidUser');
      cy.get('input[name=password]').type('invalidPassword');
      cy.get('button[type=submit]').click();
      cy.contains('Invalid credentials');
    });
  });