describe('View Product List', () => {
    it('should display the list of products', () => {
      cy.login();
      cy.visit('/admin');
      cy.get('table').should('contain', 'Product Name');
    });
  });