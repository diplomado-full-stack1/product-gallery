describe('Login Flow', () => {
    it('should log in successfully with valid credentials', () => {
      cy.visit('/login');
      cy.get('input[name=username]').type('validUser');
      cy.get('input[name=password]').type('validPassword');
      cy.get('button[type=submit]').click();
      cy.url().should('include', '/admin');
    });
  });
  