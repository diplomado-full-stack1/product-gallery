describe('Create Product', () => {
    it('should create a new product successfully', () => {
      cy.login(); // Suponiendo que tienes un comando de inicio de sesión personalizado
      cy.visit('/admin/create');
      cy.get('input[name=productName]').type('New Product');
      cy.get('input[name=price]').type('100');
      cy.get('button[type=submit]').click();
      cy.contains('Product created successfully');
    });
  });