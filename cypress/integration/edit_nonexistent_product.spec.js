describe('Edit Nonexistent Product', () => {
    it('should show an error when trying to edit a nonexistent product', () => {
      cy.login();
      cy.visit('/admin/edit/999'); // ID de un producto que no existe
      cy.contains('Product not found');
    });
  });