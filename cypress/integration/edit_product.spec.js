describe('Edit Product', () => {
    it('should edit a product successfully', () => {
      cy.login();
      cy.visit('/admin/edit/1'); // ID del producto a editar
      cy.get('input[name=price]').clear().type('150');
      cy.get('button[type=submit]').click();
      cy.contains('Product updated successfully');
    });
  });