import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
    viewportWidth: 1280,
    viewportHeight: 720,
    setupNodeEvents(on, config) {
      // Implementar eventos del nodo aquí si es necesario
    },
    specPattern: 'cypress/integration/**/*.cy.{js,jsx,ts,tsx}',
  },
});
